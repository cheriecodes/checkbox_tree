//////  JQuery Extension //////
$.fn.checkboxTree = function(jsonData){
    var stateSize = jsonData.length;
    var stateTree = '', cityTree = [], space = ' ';
    var stateData, stateName, stateNameId, citySize, showCities, stateSelectionData,
        selectedState, cities, city, cityName, cityId, selectedCity, selections, arrow;
    var resultDiv = '<div id="selection_result"></div><button id="result_btn" type="button">Show Selections</button>';
    for(var i=0; i<stateSize; i++){
        stateData = jsonData[i];
        stateName = stateData.name;
        stateNameId = stateName.replace(/\s/,'_').toLowerCase();
        citySize = stateData.children.length;
        cities = stateData.children;
        cityTree[stateNameId] = '';
        //for UI-only semi-checked state
        selections = 0;
        for (var j=0; j<citySize; j++){
            city = cities[j];
            cityName = city.name;
            cityId = cityName.replace(/\s/,'_').toLowerCase() + i + '_' + j;
            if (city.selected){
                selections +=1;
            }
            selectedCity = tools.checkboxSelection(city.selected, 0, cityId, 'cityCheckbox')[0];
            cityTree[stateNameId] += '<li class="city">' + "* " + selectedCity + space + cityName + '</li>'
        }
        stateSelectionData = tools.checkboxSelection(stateData.selected, selections, stateNameId, 'stateCheckbox');
        selectedState = stateSelectionData[0];
        arrow = stateSelectionData[1];
        showCities = stateSelectionData[2];
        stateTree += '<div class="state">' + '<span class=' + arrow + '></span>' + selectedState
            + space + stateData.name + '<ul class="cities ' + showCities + '">' + cityTree[stateNameId] + '</ul></div>'
    }
    $(this).html(stateTree);
    $(this).append(resultDiv);
    checkSelection($(this));
};

var tools = (function(){
    var space = ' ', arrow, showCities, checked, semiChecked, checkbox_template;
    var parsed, stateData,location, stateName, index, state;
    var tool_handlers = {
        capitalize: function(name){
            return name.replace(/\S\w*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
        },
        checkboxSelection: function(selected, selection, checkboxId, identifier) {
            arrow = 'down_arrow', showCities = 'hidden', checked = '', semiChecked='', checkbox_template;
            if (selected){
                checked = 'checked';
                arrow = 'right_arrow';
                showCities = '';
            }
            else if (selection > 0){
                semiChecked = 'checkbox_semi';
            }
            checkbox_template = '<div class="checkbox '+ semiChecked + '"><input type="checkbox" value="1" id=' + checkboxId + ' '
                + 'class=' + identifier + space + checked + '><label for=' + checkboxId +'></label></div>';
            return [checkbox_template, arrow, showCities];
        },
        parseData: function(data) {
            parsed = $.parseJSON(data);
            //set up state array to identify state index in stateData like a hash table
            //Arizona: 0, California: 1, Oregon: 2...
            stateData = [];
            state=[];

            $.each(parsed, function(name,info){
                //set up location
                location = {};
                location.name = name;
                if (info.selected){
                    location.selected = info.selected;
                }
                //location is a state
                if (info.parent == null){
                    location.children = [];
                    stateData.push(location);
                    state.push(name);
                }
                //location is a city
                else{
                    stateName = info.parent;
                    index = state.indexOf(stateName);
                    //state is already defined
                    if (index >= 0){
                        stateData[index].children.push(location);
                    }
                    //state is not defined yet
                    else{
                        state.push(stateName);
                        stateData.push({name: stateName, children: [location]});
                    }
                }
            });
            return stateData;
        }
    };
    return{
        capitalize: tool_handlers.capitalize,
        checkboxSelection: tool_handlers.checkboxSelection,
        parseData: tool_handlers.parseData
    };

})();

var checkSelection = (function(el){
    var $stateDiv = $('.state'), $citiesDiv;
    var checkedBoxes, citySize, $stateCheckbox, $cityCheckboxes, $cityDiv;
    var checkedStates, checkedCities, matchedName, selectedState, selectedCity, resultTemplate, stateResult, cityResult;
    var $resultDiv = $('#selection_result');

    var event_handlers = {
        toggle_arrow: function(){
            //hide the cities
            $stateDiv.on("click", ".right_arrow", function(){
                $(this).removeClass('right_arrow').addClass('down_arrow');
                $(this).siblings('.cities').slideUp();
            });
            //show the cities
            $stateDiv.on("click", ".down_arrow", function(){
                $citiesDiv = $(this).siblings('.cities');
                $(this).removeClass('down_arrow').addClass('right_arrow');
                $citiesDiv.slideDown();
                if ($citiesDiv.hasClass('hidden')){
                    $citiesDiv.removeClass('hidden');
                }
            });
        },
        checkCities: function(){
            $cityDiv = $('.city');
            $cityDiv.on('click', 'input[type=checkbox]', function(){
                $cityCheckboxes = $(this).parents('.cities').children('.city').children('.checkbox');
                $stateCheckbox = $(this).parents('.cities').siblings('.checkbox');
                citySize = $cityCheckboxes.children('input[type=checkbox]').length;
                checkedBoxes = $cityCheckboxes.children('input[type=checkbox]:checked').length;
                //check the state checkbox if all cities' checkboxes are checked as well

                if (checkedBoxes == citySize){
                    //check the state checkbox if it isn't checked yet
                    if ($(this).parents('.cities').siblings('.checkbox').children('input[type=checkbox]:not(checked)')){
                        $stateCheckbox.children('input[type=checkbox]').prop('checked', true);
                    }
                    //remove the semi check
                    if ($stateCheckbox.hasClass('checkbox_semi')){
                        $stateCheckbox.removeClass('checkbox_semi');
                    }
                }
                //semi check the state checkbox if only some cities' checkboxes are checked
                else if (checkedBoxes < citySize && checkedBoxes > 0){
                    //if the checkbox isn't checked yet
                    if (!$stateCheckbox.hasClass('checkbox_semi')){
                        $stateCheckbox.children('input[type=checkbox]').prop('checked', false);
                        $stateCheckbox.addClass('checkbox_semi');
                    }
                }
                //uncheck the state checkbox
                else if (checkedBoxes == 0){
                    if ($stateCheckbox.children('input[type=checkbox]:checked')){
                        $stateCheckbox.children('input[type=checkbox]').prop('checked', false);
                        $stateCheckbox.removeClass('checkbox_semi');
                    }
                }
            });
        },
        checkState: function(){
            $stateDiv.on('click', 'input[type=checkbox]', function(e){
                if ($(e.target).is('.stateCheckbox')){
                    if ($(this).is(':checked')){
                        $(this).parent().siblings('.cities').find('.cityCheckbox').prop('checked',true);
                        if ($(this).parent('.checkbox').hasClass('checkbox_semi')){
                            $(this).parent().removeClass('checkbox_semi');
                        }
                    }
                    else{
                        $(this).parent().siblings('.cities').find('.cityCheckbox').prop('checked',false);
                    }
                }
            });
        },
        selectionResult: function(el){
            el.on('click', '#result_btn', function(){
                //make sure all result bins are empty
                $resultDiv.html('');
                selectedState = [];
                selectedCity = [];
                checkedStates = el.find('.stateCheckbox:checked');
                checkedCities = el.find('.cityCheckbox:checked');
                $.each(checkedStates, function(i,v){
                    matchedName = v.id.match(/([a-z]+)(_([a-z]+))?/)[0].replace('_', ' ');
                    selectedState.push(tools.capitalize(matchedName));
                });
                $.each(checkedCities, function(i,v){
                    matchedName = v.id.match(/([a-z]+)(_([a-z]+))?/)[0].replace('_', ' ');
                    selectedCity.push(tools.capitalize(matchedName));
                });
                if (selectedState.length == 0){ stateResult = 'None'}
                else{
                    stateResult = selectedState.join(', ');
                }
                if (selectedCity.length == 0){ cityResult = 'None'}
                else{
                     cityResult = selectedCity.join(', ');
                }
                resultTemplate = '<p> Selected State: ' + stateResult + '</p><p> Selected City: '+ cityResult+ '</p>';
                $resultDiv.html(resultTemplate);
            });
        }
    };

    $.each(event_handlers, function(i,handler){ handler(el); });


});


$(function(){
    var jsonData;
    $.ajax({
//        url: "https://raw.github.com/cskevint/interview/master/checkbox_tree.json",
        url: "https://bitbucket.org/cheriecodes/checkbox_tree/raw/d3603262bf15b04bea3183c05f8996b3fa90d311/checkbox_tree.json",
        type: 'GET',
        jsonp: false,
        jsonpCallback: "data",
        dataType: "jsonp",
        success: function(data){
            jsonData = tools.parseData(data);
            $("#cities").checkboxTree(jsonData);
        }
    });

});
